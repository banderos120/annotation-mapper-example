<?php


namespace Annotation;


use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use Annotation\AnnotationReader;
use Annotation\AnnotationReaderInterface;
use Annotation\Method;
use DTO\CloudDTOInterface;
use Exception\CloudAnnotationException;

/**
 * Class AnnotationMapper
 * @package Mapper
 */
class AnnotationMapper implements AnnotationMapperInterface
{
    /**
     * @var AnnotationReaderInterface
     */
    protected $annotationReader;

    /**
     * @var AnnotationHandlerInterface
     */
    protected $handler;

    /**
     * AnnotationMapper constructor.
     * @param \Annotation\AnnotationReaderInterface $annotationReader
     * @param AnnotationHandlerInterface $annotationHandler
     */
    public function __construct(
        AnnotationReaderInterface $annotationReader,
        AnnotationHandlerInterface $annotationHandler
    )
    {
        $this->annotationReader = $annotationReader;
        $this->handler = $annotationHandler;
    }

    /**
     * @param string|object $class
     * @param array|null $data
     * @return mixed
     * @throws CloudAnnotationException
     */
    public function map($class, array $data = null)
    {
        if(is_string($class)){

            if(!class_exists($class)){
                throw new CloudAnnotationException(sprintf("Can't find class by name %s .", $class));
            }

            $refClass = new \ReflectionClass($class);
            $class = $refClass->newInstanceWithoutConstructor();

            unset($refClass);
        }

        $annotations = $this->annotationReader->read($class);
        $result = null;

        if($data !== null) {
            /** @var AnnotationInterface $annotation */
            foreach ($annotations as $annotation) {
                $this->handler->fromSource($annotation, $class, $data);
            }

            $result = $class;
        }else{

            $data = [];

            /** @var AnnotationInterface $annotation */
            foreach ($annotations as $annotation){
                $this->handler->fromTarget($annotation, $class, $data);
            }

            $result = $data;

        }

        unset($annotations);

        return $result;

    }

}