<?php


namespace Annotation;

use DTO\CloudDTOInterface;


/**
 * Interface AnnotationInterface
 * @package Annotation
 */
interface AnnotationInterface
{
    /**
     * @param \Reflector $reflection
     */
    public function setContext(\Reflector $reflection);

    /**
     * @return \Reflection
     */
    public function getContext();
}