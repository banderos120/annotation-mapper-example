<?php


namespace Annotation;


/**
 * Class AbstractAnnotation
 * @package Annotation
 */
abstract class AbstractAnnotation implements AnnotationInterface
{
    /**
     * @var \Reflection
     */
    protected $context;

    /**
     * @param \Reflector $reflection
     */
    public function setContext(\Reflector $reflection)
    {
        $this->context = $reflection;
    }

    /**
     * @return \Reflection
     */
    public function getContext()
    {
        return $this->context;
    }
}