<?php


namespace Annotation;


/**
 * Interface AnnotationMapperInterface
 * @package Annotation
 */
interface AnnotationMapperInterface
{
    /**
     * @param $class
     * @param array|null $data
     * @return mixed
     */
    public function map($class, array $data = null);
}