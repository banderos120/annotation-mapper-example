<?php


namespace Annotation;


interface AnnotationReaderInterface
{
    public function read($className, array $annotations = []);
}